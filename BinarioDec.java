

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.util.Scanner;

public class BinarioDec {

    public static void main(String[] b) throws IOException {

        String binario = "";
        int decimal = 0;
        
        Scanner lee = new Scanner(System.in);
        System.out.println("Introduce numero en decimal: ");
        binario = lee.nextLine();        
        
        decimal = Integer.parseInt(binario, 2); 

        System.out.println("El numero es " + decimal );

    }
}
