/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oraclecon;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author hergusalap
 */
public class DecBin {

    public static void main(String[] args) {

        int decimal;
        int exp = 0;
        int residuo = 0;
        double binario = 0;
        
        Stack<String> pila = new Stack<String>();
        
        Scanner lee = new Scanner(System.in);
        System.out.println("Introduce numero en decimal: ");
        
        decimal = lee.nextInt();

        while (decimal != 0) {
            residuo = decimal % 2;
            exp++;
            pila.push(Integer.toString(residuo));
            decimal = decimal / 2;
        }
        System.out.print("El número en binario es ");
        
        while (!pila.empty()) {
            System.out.print(pila.pop());
        }
        
        System.out.printf("\n");
    }
}
